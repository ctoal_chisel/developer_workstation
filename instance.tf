resource "aws_ebs_volume" "workspace" {
    availability_zone = "ca-central-1a"
    size = 128
    tags = {
        Name = "Developer Workspace"
    }
}

resource "aws_instance" "workstation" {
    ami = "ami-00a1484736448e060"
    instance_type = "t3.xlarge"
    subnet_id = "subnet-0c588e87ef4cfc93c"
    vpc_security_group_ids = ["sg-67299b0c", "sg-0d7782ceb5c4a870e", "sg-032c9cade92f39f56"]
    key_name = "workstation_ctoal_key"    
    root_block_device {
        volume_type="gp2"
        volume_size=64
    }

   tags = {
        Name = "Developer Workstation"
    }
}

resource "aws_volume_attachment" "workstation_workspace" {
    device_name = "/dev/sdw"
    volume_id = "${aws_ebs_volume.workspace.id}"
    instance_id = "${aws_instance.workstation.id}"
}

resource "aws_ebs_volume" "workspace2" {
    availability_zone = "ca-central-1a"
    size = 128
    tags = {
        Name = "Developer Workspace"
    }
}

resource "aws_instance" "workstation2" {
    ami = "ami-00a1484736448e060"
    instance_type = "t3.xlarge"
    subnet_id = "subnet-0c588e87ef4cfc93c"
    vpc_security_group_ids = ["sg-67299b0c", "sg-0d7782ceb5c4a870e", "sg-032c9cade92f39f56"]
    key_name = "workstation_ctoal_key"    
    root_block_device {
        volume_type="gp2"
        volume_size=64
    }

   tags = {
        Name = "Developer Workstation"
    }
}

resource "aws_volume_attachment" "workstation_workspace2" {
    device_name = "/dev/sdw"
    volume_id = "${aws_ebs_volume.workspace2.id}"
    instance_id = "${aws_instance.workstation2.id}"
}
