terraform {
    backend "s3" {
        bucket  = "chisel-infra-state"
        key     = "terraform-developer-workstation.state"
        region  = "ca-central-1"
        workspace_key_prefix = "workspaces"
    }
}
